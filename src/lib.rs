use anyhow::Result;
use mdbook::{
    book::Book,
    preprocess::{Preprocessor, PreprocessorContext},
    BookItem,
};

mod extended_table;
use extended_table::ExtendedTable;

const DEFAULT_EXTENDED_MARKDOWN_TABLE_STYLE: &str = include_str!("default_style.css");

pub struct ExtendedMarkdownTablePreprocessor {
    pub style: String,
}

impl Default for ExtendedMarkdownTablePreprocessor {
    fn default() -> Self {
        Self {
            style: DEFAULT_EXTENDED_MARKDOWN_TABLE_STYLE.to_string(),
        }
    }
}

impl Preprocessor for ExtendedMarkdownTablePreprocessor {
    fn name(&self) -> &str {
        "extended-markdown-table"
    }

    fn run(&self, _ctx: &PreprocessorContext, mut book: Book) -> Result<Book> {
        book.for_each_mut(|item: &mut BookItem| {
            if let BookItem::Chapter(ref mut chapter) = *item {
                chapter.content = self.render_extended_table_code_blocks(&chapter.content);
            }
        });

        Ok(book)
    }

    fn supports_renderer(&self, renderer: &str) -> bool {
        renderer != "not-supported"
    }
}

impl ExtendedMarkdownTablePreprocessor {
    fn render_extended_table_code_blocks(&self, contents: &str) -> String {
        let sections = parse_sections(contents);

        let mut result = String::new();

        if sections
            .iter()
            .any(|section| matches!(section, Section::TableSyntax(_)))
        {
            result += &format!("<style>{}</style>\n", self.style);
        }

        for section in sections.into_iter() {
            match section {
                Section::Unprocessed(s) => result += s,
                Section::TableSyntax(s) => {
                    result += &match ExtendedTable::parse(s).map(|table| table.into_html()) {
                        Ok(table) => table,
                        Err(e) => {
                            const CONTEXT: &str =
                                "Failed to parse an `extended-markdown-table` block!";
                            eprintln!("Warning: {}", CONTEXT);
                            eprintln!("  Reason: {}", e);
                            format!(
                                r#"<p style="color: red">{} Reason:</p><pre style="padding: 0.5em; background: #0008; color: red">{}</pre>"#,
                                CONTEXT, e
                            )
                        }
                    };
                }
            }
        }

        result
    }
}

#[derive(Debug)]
enum Section<'a> {
    Unprocessed(&'a str),
    TableSyntax(&'a str),
}

fn parse_sections(mut markdown: &str) -> Vec<Section> {
    let mut sections = vec![];

    const TABLE_START_MARKER: &str = "```extended-markdown-table";
    const TABLE_END_MARKER: &str = "```";

    while let Some(table_start) = markdown.find(TABLE_START_MARKER) {
        sections.push(Section::Unprocessed(&markdown[0..table_start]));
        markdown = &markdown[table_start + TABLE_START_MARKER.len()..];
        if let Some(table_end) = markdown.find(TABLE_END_MARKER) {
            sections.push(Section::TableSyntax(markdown[0..table_end].trim()));
            markdown = &markdown[table_end + TABLE_END_MARKER.len()..];
        } else {
            panic!("unclosed table");
        }
    }

    sections.push(Section::Unprocessed(markdown));

    sections
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sections() {
        let contents = r###"
# Test
Here is some markdown text.
```extended-markdown-table
| A table |
```
Here is some additional markdown text.
```extended-markdown-table
| Another table |
|---------------|
|   a   |   b   |
```
## Rust code
Here's some unrelated Rust code.
```rust
println!("test");
```
"###
        .trim();

        let sections = parse_sections(contents);

        assert_eq!(sections.len(), 5);

        // Note: `assert_matches` would be better here; stabilization tracked in
        // https://github.com/rust-lang/rust/issues/82775
        assert!(matches!(
            sections[0],
            Section::Unprocessed("# Test\nHere is some markdown text.\n")
        ));
        assert!(matches!(sections[1], Section::TableSyntax("| A table |")));
        assert!(matches!(
            sections[2],
            Section::Unprocessed("\nHere is some additional markdown text.\n")
        ));
        assert!(matches!(
            sections[3],
            Section::TableSyntax("| Another table |\n|---------------|\n|   a   |   b   |")
        ));
        assert!(matches!(sections[4], Section::Unprocessed("\n## Rust code\nHere's some unrelated Rust code.\n```rust\nprintln!(\"test\");\n```")));
    }
}
