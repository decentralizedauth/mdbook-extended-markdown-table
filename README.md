# mdbook-extended-markdown-table

Preprocessor for mdBook that generates tables with merged cells from ASCII text.

## Example syntax

The following table will have merged cells and styled headers exactly as expected:

```
    ```extended-markdown-table
    |          |                                      Tool                                   |
    |          |------------------|----------------------|-----------------------------------|
    |          |                  | Markdown Tables      | Extended Markdown Tables          |
    | Feature  |------------------|----------------------|-----------------------------------|
    | Support  |                  |                      | Horizontal      |        ✓        |
    |          | Merged cells     |          ✗           |-----------------|-----------------|
    |          |                  |                      | Vertical        |        ✓        |
    ```
```

## Example output

Check out the [Decentralized Auth protocol page](https://decentralizedauth.net/protocol) for several real-world examples of `mdbook-extended-markdown-table` in use.
Source code for that page is located [here](https://gitlab.com/decentralizedauth/website/-/blob/master/src/protocol.md).

## Configuration

Add the following line to your `book.toml`:

```toml
[preprocessor.extended-markdown-table]
```

and that's it!

## How to write your own tables

Be sure to use a monospaced font, as `extended-markdown-table` works using significant alignment of ASCII characters.

Any pipe character (<kbd>|</kbd>) in the source will be considered as a grid column division.
Therefore it is important to vertically align pipe characters that are intended to align in the output text.

The largest unit of grid division vertically is a single line.
Every other line is treated as a separator line; on separator lines `extended-markdown-table` will look for spaces between column dividers that are made up of dash characters (<kbd>-</kbd>).
You can make cells taller than a single row, but remember that they will be internally represented as multiple merged rows, and that they need to have an odd number of lines.
It is recommended to use a single line for each row where possible, only falling back to using multiple lines in cases where another cell on the row is split.

The first and last lines of the table should not be separator lines; they should have cell contents.

## More info

Many Unicode characters work fine in table cells.
Just keep in mind that some text editors will display some Unicode characters wider than normal characters, and some Unicode characters are actually multiple characters that get merged!
The actual character count for each row must be the same, so depending on which characters you use, it may look misaligned in your text editor.

It's possible to embed HTML inside the source for a table, as long as the vertical and horizontal separators are still aligned around the HTML code.

Markdown will _not_ be resolved within a table.
